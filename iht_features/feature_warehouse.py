import pandas as pd
import awswrangler as wr
import datetime
import logging
import numpy as np

# dataset prod.
dst_prod = "s3://klook-big-data-prod/data/processed"


def user_features(dateStr, columns=None):
    """加载用户基本信息特征。
    :params dateStr: 格式: %Y-%m-%d, 加载哪一天的活动特征，就取当天的用户状态特征
    """
    year, month, day = dateStr.split("-")
    user_base = wr.s3.read_parquet(
        path="{dst}/feature_agg/user_base/year={year}/month={month}/day={day}/".format(dst=dst_prod, year=year, month=month, day=day),
        columns=columns)
    return user_base


def device_features(dateStr, columns=None):
    """加载设备基本信息特征。
    :params dateStr: 格式: %Y-%m-%d, 加载哪一天的活动特征，就取当天的用户状态特征
    """
    year, month, day = dateStr.split("-")
    device_data = wr.s3.read_parquet(
        path="{dst}/feature_agg/user_profile_data/year={year}/month={month}/day={day}/".format(dst=dst_prod, year=year, month=month, day=day),
        columns=columns)
    # 删除top3中为-999的后段树值
    device_data['last_day_favorite_leaf_category'] = device_data['last_day_favorite_leaf_category'].replace(np.nan, '0')\
        .map(lambda x: ','.join(list(filter(lambda cate: cate != '-999', x.split(',')))))
    device_data['last_month_favorite_leaf_category'] = device_data['last_month_favorite_leaf_category'].replace(np.nan, '0')\
        .map(lambda x: ','.join(list(filter(lambda cate: cate != '-999', x.split(',')))))
    return device_data


def activity_base_features(dateStr, columns=None):
    """加载活动的基本特征。
    :params dateStr: 格式: %Y-%m-%d, 加载哪一天的活动特征，就取当天的活动状态特征
    """
    year, month, day = dateStr.split("-")
    activity_base = wr.s3.read_parquet(
        path="{dst}/feature_base/activity_base/year={year}/month={month}/day={day}/".format(dst=dst_prod, year=year, month=month, day=day),
        columns=columns)
    # 缺失值填充
    activity_base["taxon_category_id"] = activity_base["taxon_category_id"].replace(-999, 0)
    activity_base["taxon_sub_category_id"] = activity_base["taxon_sub_category_id"].replace(-999, 0)
    activity_base["taxon_leaf_category_id"] = activity_base["taxon_leaf_category_id"].replace(-999, 0)
    return activity_base


def activity_action_features(dateStr, window_size=7, feature_name_suffix="weekly_search", query="module_object_id_type=='activity'"):
    """加载活动产生的action特征。
    :params dateStr: 从那一天开始往前统计特征，格式: "%Y-%m-%d"
    :params window_size: 统计多少天的
    """
    dateRaw = datetime.datetime.strptime(dateStr, "%Y-%m-%d")

    exposure_click_list = []
    fsbr_list = []
    for i in range(window_size):
        date = dateRaw + datetime.timedelta(days=-i)
        dateStr = date.strftime("%Y-%m-%d")
        logging.info("dateStr: %s" % dateStr)
        year, month, day = dateStr.split("-")

        logging.info("load activity's exposure、click data...")
        exposure_click_daily = wr.s3.read_parquet(
            path="{dst}/feature_agg/features_exposure_click_daily/year={year}/month={month}/day={day}/".format(dst=dst_prod, year=year, month=month, day=day),
            columns=["module_object_id", "module_object_id_type", "spm_page", "spm_module","click_spm_page", "click_spm_module", "brand", "exposure_num", "click_num"])
        # filter
        exposure_click_daily = exposure_click_daily.query(query)

        exposure_click_daily = exposure_click_daily[["module_object_id", "exposure_num", "click_num"]]
        default_values = {"exposure_num": 0, "click_num": 0}
        exposure_click_daily.fillna(value=default_values, inplace=True)
        exposure_click_daily = exposure_click_daily.groupby(by=["module_object_id"], as_index=False).sum()
        exposure_click_list.append(exposure_click_daily)

        logging.info("load activity's favourite、shopping、booking、review data...")
        fsbr_daily = wr.s3.read_parquet(
            path="{dst}/feature_agg/features_favourite_shopping_booking_review_daily/year={year}/month={month}/day={day}/".format(dst=dst_prod, year=year, month=month, day=day),
            columns=["activity_id", "favourite_num", "shopping_num", "booking_num", "average_review_score"])
        fsbr_list.append(fsbr_daily)

    # merge
    exposure_click_weekly = pd.concat(exposure_click_list, ignore_index=True)
    exposure_click_weekly = exposure_click_weekly.groupby(by=["module_object_id"], as_index=False).sum()
    rename_columns = {"module_object_id": "activity_id",
                      "exposure_num": "exposure_num_{msg}".format(msg=feature_name_suffix),
                      "click_num": "click_num_{msg}".format(msg=feature_name_suffix)}
    exposure_click_weekly.rename(columns=rename_columns, inplace=True)

    fsbr_weekly = pd.concat(fsbr_list, ignore_index=True)
    default_values = {"favourite_num": 0, "shopping_num": 0, "booking_num": 0, "average_review_score": 0}
    fsbr_weekly.fillna(value=default_values, inplace=True)
    fsb_weekly = fsbr_weekly[["activity_id", "favourite_num", "shopping_num", "booking_num"]]
    fsb_weekly = fsb_weekly.groupby(by="activity_id", as_index=False).sum()
    r_weekly = fsbr_weekly[["activity_id", "average_review_score"]]
    r_weekly = r_weekly.groupby(by="activity_id", as_index=False).mean()
    fsbr_weekly = pd.merge(fsb_weekly, r_weekly, on=["activity_id"], how="left")
    rename_columns = {"favourite_num": "favourite_num_{msg}".format(msg=feature_name_suffix),
                      "shopping_num": "shopping_num_{msg}".format(msg=feature_name_suffix),
                      "booking_num": "booking_num_{msg}".format(msg=feature_name_suffix),
                      "average_review_score": "average_review_score_{msg}".format(msg=feature_name_suffix)}
    fsbr_weekly.rename(columns=rename_columns, inplace=True)

    fsbr_weekly["activity_id"] = fsbr_weekly["activity_id"].apply(str)
    return pd.merge(exposure_click_weekly, fsbr_weekly, on=["activity_id"], how="outer")
