import os


def _read_lines_from_file(file):
    item_2_id = {"": 0}
    lines = [line.strip().lower() for line in open(file, "r").readlines() if line.strip() != ""]
    item_2_id.update(dict([(str(line), index+1) for index, line in enumerate(lines)]))
    return item_2_id


def _read_geo_from_file(file):
    """加载country/city的词典
    """
    index = 1

    item_2_id = {"": 0}
    item = []
    for line in open(file):
        line = line.strip().lower()
        if line != "":
            item.append(line)
        else:
            if item == []:
                continue
            item = map(lambda x: (x, index), item)
            item_2_id.update(dict(item))
            item = []
            index += 1
    if item:
        item = map(lambda x: (x, index), item)
        item_2_id.update(dict(item))
    return item_2_id


def load_dictionary():
    """加载label词典.
    """
    gender = _read_lines_from_file(os.path.join(os.path.dirname(__file__), "gender.txt"))
    brand = _read_lines_from_file(os.path.join(os.path.dirname(__file__), "brand.txt"))
    currency = _read_lines_from_file(os.path.join(os.path.dirname(__file__), "currency.txt"))
    platform = _read_lines_from_file(os.path.join(os.path.dirname(__file__), "platform.txt"))
    language = _read_lines_from_file(os.path.join(os.path.dirname(__file__), "language.txt"))
    signup_client = _read_lines_from_file(os.path.join(os.path.dirname(__file__), "signup_client.txt"))
    user_grouping = _read_lines_from_file(os.path.join(os.path.dirname(__file__), "user_grouping.txt"))
    favorite_payment_channel = _read_lines_from_file(os.path.join(os.path.dirname(__file__), "favorite_payment_channel.txt"))
    register_client = _read_lines_from_file(os.path.join(os.path.dirname(__file__), "register_client.txt"))
    user_type = _read_lines_from_file(os.path.join(os.path.dirname(__file__), "user_type.txt"))
    brand_os = _read_lines_from_file(os.path.join(os.path.dirname(__file__), "os.txt"))


    ip_country_code = _read_geo_from_file(os.path.join(os.path.dirname(__file__), "ip_country_name.txt"))  # name中包含code
    ip_city_name = _read_geo_from_file(os.path.join(os.path.dirname(__file__), "ip_city_name.txt"))

    labelEncoder_dict = {"gender": gender, "brand": brand,
                         "currency": currency, "platform": platform,
                         "language": language, "signup_client": signup_client,
                         "user_grouping": user_grouping, "locale": language,
                         "ip_country_code": ip_country_code, "ip_city_name": ip_city_name,
                         "favorite_payment_channel": favorite_payment_channel,
                         "favorite_currency": currency, "lastest_platform": platform,
                         "permanent_city": ip_city_name, "country": ip_country_code,
                         "register_client": register_client, "user_type": user_type, "os": brand_os}
    return labelEncoder_dict


language_mapping = {
    "de": "de_DE",
    "en": "en_BS",
    "es": "es_ES",
    "fr": "fr_FR",
    "id": "id_ID",
    "it": "it_IT",
    "ja": "ja_JP",
    "ko": "ko_KR",
    "ru": "ru_RU",
    "th": "th_TH",
    "vi": "vi_VN",
    "en-AU": "en_AU",
    "en-CA": "en_CA",
    "en-GB": "en_GB",
    "en-HK": "en_HK",
    "en-IN": "en_IN",
    "en-MY": "en_MY",
    "en-NZ": "en_NZ",
    "en-PH": "en_PH",
    "en-SG": "en_SG",
    "en-US": "en_US",
    "en-BS": "en_BS",
    "zh-CN": "zh_CN",
    "zh-HK": "zh_HK",
    "zh-TW": "zh_TW"
}


def encoder(X, labelEncoder_dict, cols=[]):
    """将类别特征转换成0～n_classes-1之间的标签。

    X: DataFrame()

    :param X: DataFrame, 数据
    :param cols: list, 要进行处理LaberEncoder的类别列名, 如果为空，则和labelEncoder_dict中存在的所有col进行处理
    :param labelEncoder_dict: dict, 每个cname下面是dict，里面是key-value对, 如:
        如: X = pd.DataFrame([["a"],["b"],["c"],["d"]], columns=["category_id"])
            labelEncoder_dict["category_id"] = {"a":1, "b":2, "c":3, "":0}
    """
    if cols == []:
        cols = X.columns.values.tolist()

    # 字段统一
    if "language" in cols:
        X["language"] = X["language"].apply(lambda x: language_mapping[x] if x in language_mapping else x)
    if "locale" in cols:
        X["locale"] = X["locale"].apply(lambda x: language_mapping[x] if x in language_mapping else x)

    le_dict = labelEncoder_dict

    def convert_id(x, cname):
        try:
            if x in le_dict[cname]:
                return le_dict[cname][x]
            else:
                return le_dict[cname][""]
        except Exception as e:
            return le_dict[cname][""]

    for cname in cols:
        if cname not in le_dict:
            continue
        X[cname] = X[cname].apply(lambda x: convert_id(str(x).lower(), cname))
    return X
