from feature_warehouse import *
# from s3Utils import upload_file_to_s3
from iht_features.labelEncoder import load_dictionary, encoder

import langid
import argparse
import datetime
import logging
import pandas as pd
import numpy as np

LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

labelEncoder_dict = load_dictionary()


def gen_user_features():
    """生成用户特征数据，T+1
    Index(['id', 'gender', 'country', 'locale', 'credit_balance',
       'register_client', 'user_type', 'user_grouping', 'marital_status',
       'kid_bearing', 'permanent_city', 'lastest_platform',
       'lifetime_value_hkd', 'last_month_value_hkd', 'last_halfyear_value_hkd',
       'lifetime_refund_value_hkd', 'last_month_refund_value_hkd',
       'last_halfyear_refund_value_hkd', 'lifetime_orders',
       'last_month_orders', 'last_halfyear_orders',
       'lifetime_average_value_hkd', 'last_month_average_value_hkd',
       'last_halfyear_average_value_hkd', 'last_week_favourite_num',
       'last_month_favourite_num', 'last_week_shopping_num',
       'last_month_shopping_num', 'lifetime_gift_card_value_usd',
       'successful_referrals', 'favorite_currency',
       'favorite_payment_channel'],
      dtype='object')
    """
    logging.info("gen user features...")
    dateRaw = datetime.datetime.now() + datetime.timedelta(days=-1)
    dateStr = dateRaw.strftime("%Y-%m-%d")
    # 用户基本信息
    columns = ["global_id", "gender", "country", "locale", "credit_balance",
               "register_client", "user_type", "user_grouping", "marital_status",
               "kid_bearing", "permanent_city", "lastest_platform",
               "lifetime_value_hkd", "last_month_value_hkd", "last_halfyear_value_hkd",
               "lifetime_refund_value_hkd", "last_month_refund_value_hkd",
               "last_halfyear_refund_value_hkd", "lifetime_orders",
               "last_month_orders", "last_halfyear_orders",
               "lifetime_average_value_hkd", "last_month_average_value_hkd",
               "last_halfyear_average_value_hkd", "last_week_favourite_num",
               "last_month_favourite_num", "last_week_shopping_num",
               "last_month_shopping_num", "lifetime_gift_card_value_usd",
               "successful_referrals", "favorite_currency",
               "favorite_payment_channel", "last_update_time"]
    user_data = user_features(dateStr, columns=columns)
    user_data.rename(columns={"global_id": "id"}, inplace=True)
    columns_name = ["id"] + [cname for cname in user_data.columns.tolist() if cname not in ["id", "last_update_time"]]
    user_data = user_data.loc[:, columns_name].copy()

    # 数值转化
    user_data = encoder(user_data, labelEncoder_dict)
    user_data.replace(np.nan, 0, inplace=True)
    for cname in user_data.select_dtypes(include="bool").columns:
        user_data[cname] = user_data[cname].astype("int")
    return user_data


def gen_device_features():
    """生成设备特征数据，T+1
    Index(['id', 'last_week_searchpv', 'last_month_searchpv',
       'last_week_click_num', 'last_month_click_num'],
      dtype='object')
    """
    logging.info("gen device features...")
    dateRaw = datetime.datetime.now() + datetime.timedelta(days=-1)
    dateStr = dateRaw.strftime("%Y-%m-%d")
    # 设备基本信息
    columns = ["device_id", "last_week_searchpv", "last_month_searchpv", "last_week_click_num",
               "last_month_click_num", "last_update_time", "last_day_favorite_leaf_category",
               "last_month_favorite_leaf_category"]
    device_data = device_features(dateStr, columns=columns)
    # 临时方案，last_day_favorite_leaf_category/last_month_favorite_leaf_category特征 只取top 1
    device_data['last_day_favorite_leaf_category_top_1'] = device_data['last_day_favorite_leaf_category']\
        .map(lambda x: str(x).split(',')[0]).astype(int).replace(-999, 0)
    device_data['last_month_favorite_leaf_category_top_1'] = device_data['last_month_favorite_leaf_category']\
        .map(lambda x: str(x).split(',')[0]).astype(int).replace(-999, 0)
    device_data.rename(columns={"device_id": "id"}, inplace=True)
    columns_name = ["id"] + [cname for cname in device_data.columns.tolist() if cname not in ["id", "last_update_time"]]
    device_data = device_data.loc[:, columns_name].copy()
    return device_data


def gen_activity_features():
    """生成活动特征，T+1的特征
    如果有新的特征，只需要根据activity_id把新特征pd.merge到activity_features上，how必须选择outer

    Index(['id', 'category_id', 'activity_country_id', 'activity_city_id','has_video',
          'taxon_category_id', 'taxon_sub_category_id', 'taxon_leaf_category_id',
          'exposure_num_weekly_search', 'click_num_weekly_search',
          'favourite_num_weekly_search', 'shopping_num_weekly_search',
          'booking_num_weekly_search', 'average_review_score_weekly_search',
          'exposure_num_monthly_search', 'click_num_monthly_search',
          'favourite_num_monthly_search', 'shopping_num_monthly_search',
          'booking_num_monthly_search', 'average_review_score_monthly_search',
          'exposure_num_weekly_recomCity', 'click_num_weekly_recomCity',
          'favourite_num_weekly_recomCity', 'shopping_num_weekly_recomCity',
          'booking_num_weekly_recomCity', 'average_review_score_weekly_recomCity',
          'exposure_num_monthly_recomCity', 'click_num_monthly_recomCity',
          'favourite_num_monthly_recomCity', 'shopping_num_monthly_recomCity',
          'booking_num_monthly_recomCity',
          'average_review_score_monthly_recomCity'],
      dtype='object')
    """
    logging.info("gen activity features...")
    dateRaw = datetime.datetime.now() + datetime.timedelta(days=-1)
    dateStr = dateRaw.strftime("%Y-%m-%d")
    # 活动基本特征
    activity_features = activity_base_features(dateStr, columns=["activity_id", "category_id", "activity_country_id",
                                                                 "activity_city_id","has_video", "taxon_category_id",
                                                                 "taxon_sub_category_id", "taxon_leaf_category_id"])
    activity_features["activity_id"] = activity_features["activity_id"].apply(str)

    ########## search ##########
    search_query = "spm_page in ('SearchResult') and spm_module in ('SearchResults_LIST', 'Activity_LIST')"
    for window_size, message in [(7, "weekly_search"), (30, "monthly_search")]:
        action_features_search = activity_action_features(
            dateStr, window_size=window_size, feature_name_suffix=message, query=search_query)
        activity_features = pd.merge(activity_features, action_features_search, on=["activity_id"], how="outer")

    ########## recommendation city ##########
    recom_city_query = '(spm_page in ("City","CityActivityListing") and spm_module == "Activity_LIST")\
                        or\
                        (click_spm_page in ("City","CityActivityListing") and click_spm_module=="Category_LIST" and \
                        spm_page in ("SearchResult","Experience_Vertical","Experience_SubVertical") and \
                        spm_module in ("Activity_LIST","SearchResults_LIST","ThemeActivity_LIST"))'
    for window_size, message in [(7, "weekly_recomCity"), (30, "monthly_recomCity")]:
        action_features_recomCity = activity_action_features(
            dateStr, window_size=window_size, feature_name_suffix=message, query=recom_city_query)
        activity_features = pd.merge(activity_features, action_features_recomCity, on=["activity_id"], how="outer")



    # 数值转换
    activity_features.rename(columns={"activity_id": "id"}, inplace=True)
    columns_name = ["id"] + [cname for cname in activity_features.columns.tolist() if cname not in ["id"]]
    activity_features = activity_features.loc[:, columns_name].copy()
    activity_features.replace(np.nan, 0, inplace=True)
    for cname in activity_features.select_dtypes(include="bool").columns:
        activity_features[cname] = activity_features[cname].astype("int")
    return activity_features


def gen_online_feature_mapping():
    logging.info("gen online feature mapping...")

    online_features = ["ip_country_code", "ip_city_name", "platform", "language",  "currency", "brand", "os"]
    items_mapping = []
    for feature_name in online_features:
        logging.info("processing feature name: %s" % feature_name)
        if feature_name not in labelEncoder_dict:
            logging.error("not found feature: %s" % feature_name)
        items = labelEncoder_dict[feature_name].items()
        if feature_name == "ip_country_code":
            items_tmp = []
            for item in items:
                value, id = item
                if len(value) > 2 or len(value) == 1:  # 只留0和2
                    continue
                language = langid.classify(value)[0]
                if language == "zh":
                    continue
                items_tmp.append((value, id))
            items = items_tmp
        if feature_name == "ip_city_name":
            items_tmp = []
            for item in items:
                value, id = item
                language = langid.classify(value)[0]
                if language == "zh":
                    continue
                items_tmp.append((value, id))
            items = items_tmp
        items = [("{ftname}-{ftvalue}".format(ftname=feature_name, ftvalue=item[0] if item[0] != "" else "default"),
                 item[1]) for item in items]
        items_mapping.extend(items)
    return pd.DataFrame(items_mapping, columns=["name", "id"])


def main(**params):
    if params["feature_type"] == "activity":
        local_file = "./results/activityFeature.csv"
        activity_features = gen_activity_features()
        if len(activity_features) < 10000:
            raise ValueError("activity features size is less than 10000.")
            return
        logging.info("activity features size: %s." % len(activity_features))
        activity_features.to_csv(local_file, index=False)

        # object_name = "search/data/ranking/feature/activityFeature.csv"
        # logging.info("upload features to s3({env}): {obj}".format(env=params["env"], obj=object_name))
        # upload_file_to_s3(local_file, object_name=object_name, env=params["env"])

    elif params["feature_type"] == "user":
        local_file = "./results/userFeature.csv"
        user_features = gen_user_features()
        if len(user_features) < 5000000:
            raise ValueError("user features size is less than 5000000.")
            return
        logging.info("user features size: %s." % len(user_features))
        user_features.to_csv(local_file, index=False)

        # object_name = "search/data/ranking/feature/userFeature.csv"
        # logging.info("upload features to s3({env}): {obj}".format(env=params["env"], obj=object_name))
        # upload_file_to_s3(local_file, object_name=object_name, env=params["env"])

    elif params["feature_type"] == "device":
        local_file = "./results/deviceFeature.csv"
        device_features = gen_device_features()
        if len(device_features) < 10000000:
            raise ValueError("device features size is less than 10000000.")
            return
        logging.info("device features size: %s." % len(device_features))
        device_features.to_csv(local_file, index=False)

        # object_name = "search/data/ranking/feature/deviceFeature.csv"
        # logging.info("upload features to s3({env}): {obj}".format(env=params["env"], obj=object_name))
        # upload_file_to_s3(local_file, object_name=object_name, env=params["env"])

    elif params["feature_type"] == "featuremapping":
        local_file = "./results/onlineFeatureMapping.csv"
        online_feature_mapping = gen_online_feature_mapping()
        if len(online_feature_mapping) == 0:
            raise ValueError("feature mapping pair size: 0.")
            return
        logging.info("feature mapping pair size: %s." % len(online_feature_mapping))
        online_feature_mapping.to_csv(local_file, index=False)

        object_name = "search/data/ranking/config/onlineFeatureMapping.csv"
        logging.info("upload features to s3({env}): {obj}".format(env=params["env"], obj=object_name))
        upload_file_to_s3(local_file, object_name=object_name, env=params["env"])

    else:
        pass
    logging.info("finished.")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--feature_type", default=None, help="user、device、activity or featuremapping.", type=str)
    parser.add_argument("--env", default="test", help="test or prod.", type=str)
    args = parser.parse_args()
    main(**{"feature_type": args.feature_type, "env": args.env})
