import logging
from xgboost import DMatrix, train, Booster
import pickle
import numpy as np
import os
import math


def get_activity_index(x):
    """根据platform返回活动所在的moudle_index

    :param x:{'platform':_, 'page_extra':_, 'module_extra':_,'module_list_len':_,'module_list_index':_ }
    """
    activity_index = -100
    try:
        if x['platform'] in ('desktop', 'mobile'):
            if 'page' in dict(x['page_extra']):
                activity_index = int(dict(x['page_extra'])['page']) * int(x['module_list_len']) + int(x['module_list_index'])
                return activity_index
            elif 'page' in dict(x['module_extra']):
                activity_index = int(dict(x['module_extra'])['page']) * int(x['module_list_len']) + int( x['module_list_index'])
                return activity_index
        else:
            # app 直接返回结果
            return int(x['module_list_index'])
    except:
        # 如果解析页码或者module_list_index不成功，返回-100
        return activity_index

def get_dcg(dcg_type, top, input_df):
    input_df = input_df.query('score_rank<=@top')
    input_df['dcg_log2'] = input_df['score_rank'].map(lambda x : math.log2(x+1))
    input_df['dcg_'+dcg_type] = input_df[dcg_type]/input_df['dcg_log2']
    input_df.fillna(0, inplace=True)
    return sum(input_df['dcg_'+dcg_type])

def get_dcg_res(exp_res, def_res, dcg_type_list, model_name):
    dcg_type_name = []
    dcg = []
    default = []
    dcg_value = []
    for dcg_type in dcg_type_list:
        for top in (5, 10, 20, 50, 100, 200):
            exp = get_dcg(dcg_type, top, exp_res)
            con = get_dcg(dcg_type, top, con_res)
            dcg_type_name.append(dcg_type)
            dcg.append('dcg@'+str(top))
            default.append(con)
            dcg_value.append(exp)
    return {'dcg_type': dcg_type_name, 'dcg@': dcg, 'default': default, model_name: dcg_value}



def train_model(dtrain, evals, model_file, save_model):
    params = {
        "booster": "gbtree",
        "objective": "rank:pairwise",
        "max_depth": 4,
        "silent": 0,
        "learning_rate": 0.1,
        "nthread": -1,
        "eval_metric": ["auc", "ndcg"]
    }

    logging.info("train shape: [%s, %s]" % (dtrain.num_row(), dtrain.num_col()))

    # 模型训练
    logging.info("train model...")
    evals_result = {}
    xgb_model = None
    gbm = train(params, dtrain, evals=evals, xgb_model=xgb_model, num_boost_round=500, early_stopping_rounds=None,evals_result=evals_result)

    # 保存模型
    if save_model:
        gbm.save_model(model_file)
        logging.info("save model to %s." % model_file)

    return evals_result, gbm, params


def generate_dataset(df):
    """生成数据集.
    """
    # df数据分布: 0,label/y; 1,qid; 2~n,features
    feature_names = df.columns.values.tolist()[2:]
    data = df.to_numpy()

    y = data[:, 0]
    qid = data[:, 1]
    X = data[:, 2:]

    # 数据分组
    _, group_sizes = np.unique(qid, return_counts=True)
    indices = qid.argsort()  # 从小到大排序

    X = X[indices]
    y = y[indices]

    outputs = DMatrix(X, label=y, feature_names=feature_names, missing=0)
    outputs.set_group(group_sizes)
    return outputs


def print_result(results):
    print_str = "eval_set: %s, eval_metric: %s, max value: %s, final value: %s."
    eval_result_items = []
    for eval_set in results:
        items = []
        for eval_metric in results[eval_set]:
            values = results[eval_set][eval_metric]
            max_value = max(values)
            final_value = values[-1]
            logging.info(print_str % (eval_set, eval_metric, max_value, final_value))
            items.append({
                "eval_metric": eval_metric,
                "max_value": max_value,
                "final_value": final_value
            })
        eval_result_items.append({
            "evalSet_name": eval_set,
            "metrics_items": items
        })
    return eval_result_items

def load_model(model_path):
    """加载模型训练好的模型
    :param model_path: 模型路径
    """
    model_file = os.path.join(model_path, "xgboost.model")
    logging.info("load model from %s." % model_file)
    model = Booster(model_file=model_file)
    # 加载feature_names，按训练顺序整理特征
    feature_names_file = os.path.join(model_path, "feature_names.pkl")
    logging.info("load feature names from %s" % feature_names_file)
    with open(feature_names_file, "rb") as f:
        feature_names = pickle.load(f)
    return model, feature_names