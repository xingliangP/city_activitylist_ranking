from iht_features.labelEncoder import load_dictionary, encoder
from iht_features import feature_warehouse
from xgboost_ranking import load_city_list_sample
import time
import logging
import datetime
import random
import pandas as pd
import numpy as np
import os
import gc

LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)


def load_train_data(cur_date_str, days_num=30):
    """加载训练集和验证集的特征数据

    :param cur_date_str: 训练集窗口最后一天
    :param days_num: 训练集窗口大小
    """
    logging.info("loading train data...")
    load_start_time = time.time()
    iht_data = pd.DataFrame()
    dateRaw = datetime.datetime.strptime(cur_date_str, "%Y-%m-%d")
    booking_base = load_city_list_sample.load_booking_data(cur_date_str)
    for i in list(range(days_num))[::-1]:
        dateRaw_in = dateRaw + datetime.timedelta(days=-i)
        date_str_in = dateRaw_in.strftime("%Y-%m-%d")
        # 加载点击曝光样本
        sub_iht_data = load_city_list_sample.load_sample_data(date_str_in)
        act_base_columns = ['activity_id', 'activity_country_id', 'activity_city_id', 'has_video',
                            'taxon_category_id', 'taxon_sub_category_id', 'taxon_leaf_category_id']
        # 加载booking label
        booking_base_sub = booking_base.query('date==@date_str_in')
        sub_iht_data = pd.merge(sub_iht_data, booking_base_sub[['uid', 'activity_id', 'booking_label']], on=['uid', 'activity_id'], how='left')
        sub_iht_data['label'] = sub_iht_data[['booking_label', 'label']].apply(lambda x: 2 if x['booking_label'] == 2 else x['label'], axis=1)
        sub_iht_data = sub_iht_data.drop(columns=['uid', 'booking_label'])

        # 获取商品静态特征；输入的时间需要做T-1
        act_base_in = dateRaw_in + datetime.timedelta(days=-1)
        act_base_str = act_base_in.strftime("%Y-%m-%d")
        act_base = feature_warehouse.activity_base_features(act_base_str, columns=act_base_columns)
        act_base['activity_id'] = act_base['activity_id'].astype(str)

        # 获取用户画像数据； 输入的时间无需要做T-1，该表数据在生成时已为T-1
        profile_columns = ['device_id', 'last_day_favorite_leaf_category', 'last_month_favorite_leaf_category']
        user_profile = feature_warehouse.device_features(date_str_in, columns=profile_columns)

        # 获取商品动态特征；活动的action需要做滑窗，输入的时间需要做t-1
        date_action_in = dateRaw_in + datetime.timedelta(days=-1)
        date_action_str = date_action_in.strftime("%Y-%m-%d")
        # 限制数据为城市活动页的spm
        data_query = '(spm_page in ("City","CityActivityListing") and spm_module == "Activity_LIST")'
        act_action_weekly = feature_warehouse.activity_action_features(date_action_str, 7, feature_name_suffix='weekly_recomCity', query=data_query)
        act_action_monthly = feature_warehouse.activity_action_features(date_action_str, 30, feature_name_suffix='monthly_recomCity', query=data_query)

        # 组装所有特征
        sub_iht_data = pd.merge(sub_iht_data, user_profile, on='device_id', how='left')
        sub_iht_data = pd.merge(sub_iht_data, act_base, on=['activity_id'], how='left')
        sub_iht_data = pd.merge(sub_iht_data, act_action_weekly, on=['activity_id'], how='left')
        sub_iht_data = pd.merge(sub_iht_data, act_action_monthly, on=['activity_id'], how='left')
        sub_iht_data['date'] = date_str_in

        # 打印当天特征样本集特征纬度，并保存到s3备用。
        logging.info("loading day data shape is (%d,%d)" % (sub_iht_data.shape[0], sub_iht_data.shape[1]))
        save_path_prefix = "s3://sagemaker-studio-227729285155-ml/hubery-peng/cityActivity_ranking/data/train_data/" + date_str_in
        sub_iht_data.to_parquet(save_path_prefix + "/new_group_train_data.parquet")
        iht_data = iht_data.append(sub_iht_data)
    del booking_base, sub_iht_data
    gc.collect()

    # 缺失值补全
    iht_data['last_day_favorite_leaf_category_top_1'] = iht_data['last_day_favorite_leaf_category'].replace(np.nan, '0').replace('', '0') \
        .map(lambda x: str(x).split(',')[0]).astype(int)
    iht_data['last_month_favorite_leaf_category_top_1'] = iht_data['last_month_favorite_leaf_category'].replace(np.nan, '0').replace('', '0') \
        .map(lambda x: str(x).split(',')[0]).astype(int)

    # 缺失值填充
    default_values = {"platform": '',
                      "language": '',
                      "ip_country_code": '',
                      "ip_city_name": '',
                      'activity_country_id': 0,
                      'activity_city_id': 0,
                      'taxon_category_id': 0,
                      'taxon_sub_category_id': 0,
                      'taxon_leaf_category_id': 0}
    iht_data.fillna(value=default_values, inplace=True)

    # 以下字段fillna不能替换缺失值，需要用replace，后期定位原因
    fillna_col = ['exposure_num_weekly_recomCity', 'click_num_weekly_recomCity', 'shopping_num_weekly_recomCity',
                  'favourite_num_weekly_recomCity', 'booking_num_weekly_recomCity', 'average_review_score_weekly_recomCity',
                  'exposure_num_monthly_recomCity', 'click_num_monthly_recomCity', 'shopping_num_monthly_recomCity',
                  'favourite_num_monthly_recomCity', 'booking_num_monthly_recomCity', 'average_review_score_monthly_recomCity']
    for col in fillna_col:
        iht_data[col].replace(np.nan, 0.0, inplace=True)
    iht_data['has_video'].replace(np.nan, False, inplace=True)

    # 将未切割的训练数据到s3，便于后期调试，保持模型路径为训练集合的起始时间。<s3路径由processing中传入>
    dateRaw = datetime.datetime.strptime(cur_date_str, "%Y-%m-%d")
    start_data = dateRaw + datetime.timedelta(days=-days_num)
    start_data = start_data.strftime("%Y-%m-%d")
    model_prefile = start_data + '_' + cur_date_str
    model_path = "./results/models/"+model_prefile
    if not os.path.exists(model_path):
        os.makedirs(model_path)
    save_path = model_path + '/all_train_data.parquet'
    logging.info("train data save path is : %s" % save_path)
    iht_data.to_parquet(save_path)

    # 切分数据集: train:test = 8:2
    logging.info("Divide the sample...")
    device_ids = iht_data["page_open_id"].unique()
    random.shuffle(device_ids)
    test_rate = 0.2
    test_size = int(len(device_ids) * test_rate)
    test_data = iht_data[iht_data["page_open_id"].isin(device_ids[:test_size])].reset_index(drop=True)
    train_data = iht_data[iht_data["page_open_id"].isin(device_ids[test_size:])].reset_index(drop=True)

    # 保存分割后的训练和验证数据到s3,便于后期调试
    train_data.to_parquet(model_path + '/split_train_data.parquet')
    test_data.to_parquet(model_path + '/split_test_data.parquet')

    # 字段顺序调整
    columns = iht_data.columns.values.tolist()
    columns = [column for column in columns if
               column not in ["label", "page_open_id", 'device_id', 'last_day_favorite_leaf_category',
                              'last_month_favorite_leaf_category']]
    columns = ["label", "page_open_id"] + columns
    train_data = train_data[columns]
    test_data = test_data[columns]

    # 类别特征编码
    labelEncoder_dict = load_dictionary()
    train_data = encoder(train_data, labelEncoder_dict)
    test_data = encoder(test_data, labelEncoder_dict)

    # 删除指定列
    delete_columns = ["activity_id"]
    train_data = train_data.drop(columns=delete_columns)
    test_data = test_data.drop(columns=delete_columns)

    # 打印训练集与验证集生成时间
    logging.info("load train data end")
    load_end_time = time.time()
    logging.info('load train data 总时时长: %.2f 分钟' % ((load_end_time - load_start_time) / 60))
    return (train_data, test_data)