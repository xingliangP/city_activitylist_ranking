import os
import pandas as pd
import datetime
from __init__ import get_activity_index
import logging
pd.options.mode.chained_assignment = None

dst_prod = "s3://klook-big-data-prod/data/processed"


def load_sample_data(dateStr):
    """加载城市活动页面dateStr样本

    :param dateStr: 样本日期
    """
    logging.info('loading %s sample data ...' % dateStr)
    year, month, day = dateStr.split("-")
    dict_lang = {'de': 'de_DE', 'es': 'es_ES', 'fr': 'fr_FR', 'id': 'id_ID', 'it': 'it_IT', 'ja': 'ja_JP',
                 'ru': 'ru_RU', 'vi': 'vi_VN', 'th': 'th_TH', 'ko': 'ko_KR', 'en': 'en_BS'}
    # 数据读取过滤--数据加载时过滤，减小数据量，提升整体处理速度
    data_source_1 = [('spm_page', 'in', ("City", "CityActivityListing")), ('spm_module', '=', 'Activity_LIST'),
                     ('module_object_id_type', '=', 'activity'), ('module_object_id', '!=', ''), ('site_name', 'in', ("klook", "www.klook.com"))]
    # 数据加载后过滤 --这部分数据量比较少，加载后过滤
    data_filter_1 = 'device_id.notnull() and device_id !=""  and page_open_id.notnull() and page_open_id !=""'
    # 正负样本公共特征--输入特征名称
    input_columns = ["device_id", "uid", "platform", "language", "ip_country_code", 'ip_city_name', 'module_object_id', 'page_open_id',
                     'page_extra', 'module_extra', 'module_list_index', 'module_list_len']
    # 正负样本公共特征 --输出特征名称 + 'label'
    output_features = ['page_open_id', 'activity_id', 'device_id', 'uid', 'platform', 'language', 'ip_country_code', 'ip_city_name']

    # sh--下载曝光数据
    sh_exp_download = "aws s3 cp {dst}/in-house-tracking/event_exposure/year={year}/month={month}/day={day}/ \
                       ./dataset/event_exposure/year={year}/month={month}/day={day} --recursive  > cp.log ".format(dst=dst_prod, year=year, month=month, day=day)
    # sh--删除空文件夹
    sh_delete_null_file = "find ./dataset/* -name \"*\" -type f -size 0c  | xargs -n 1 rm -f"
    pd_exposure = pd.DataFrame()
    if os.system(sh_exp_download) == 0:
        if os.system(sh_delete_null_file) == 0:
            cur_path = './dataset/event_exposure/year={year}/month={month}/day={day}'.format(year=year, month=month,day=day)
            pd_exposure = pd.read_parquet(cur_path, columns=input_columns + ['duration'], filters=data_source_1)
            # 删除本地文件，释放磁盘空间
            sh_delete_file = 'rm -rf ./dataset/event_exposure/'
            os.system(sh_delete_file)
            data_filter_exp = data_filter_1 + 'and duration>1000'
            pd_exposure = pd_exposure.query(data_filter_exp, engine='python')
            # 语言多端统一
            pd_exposure.loc[:, 'language'] = pd_exposure['language'].map(lambda x: x.replace("-", "_")).map(lambda x: dict_lang[x] if x in dict_lang else x)
            # 增加activity_index字段。目前的埋点系统<2021.03.24>取值方法如下：
            # page字段：1.desktop端从page_extra,mobile端从module_extra；2.app端无page字段。index字段：module_list_index
            # 确保page_extra，module_extra都为list格式。
            pd_exposure['page_extra'] = pd_exposure['page_extra'].map(lambda x: [('no_page', -1)] if type(x) != list else x)
            pd_exposure['module_extra'] = pd_exposure['module_extra'].map(lambda x: [('no_page', -1)] if type(x) != list else x)
            pd_exposure['activity_index'] = pd_exposure[['platform', 'page_extra', 'module_extra', 'module_list_index', 'module_list_len']]\
                .apply(lambda x: get_activity_index(x), axis=1)
            pd_exposure = pd_exposure.rename(columns={'module_object_id': 'activity_id'})
            # 选择指定字段
            pd_exposure = pd_exposure[['activity_index']+output_features].drop_duplicates()

    # sh--下载点击数据
    sh_click_download = "aws s3 cp {dst}/in-house-tracking/event_click/year={year}/month={month}/day={day}/ \
                       ./dataset/event_click/year={year}/month={month}/day={day} --recursive > cp.log".format(dst=dst_prod, year=year, month=month, day=day)
    pd_action = pd.DataFrame()
    if os.system(sh_click_download) == 0:
        if os.system(sh_delete_null_file) == 0:
            cur_path = './dataset/event_click/year={year}/month={month}/day={day}'.format(year=year, month=month, day=day)
            # 取城市数据
            pd_action = pd.read_parquet(cur_path, columns=input_columns, filters=data_source_1)
            # 删除本地文件，释放磁盘空间
            sh_delete_file = 'rm -rf ./dataset/event_click/'
            os.system(sh_delete_file)
            pd_action = pd_action.query(data_filter_1, engine='python')
            # 语言多端统一
            pd_action.loc[:, 'language'] = pd_action['language'].map(lambda x: x.replace("-", "_")).map(lambda x: dict_lang[x] if x in dict_lang else x)
            # 增加activity_index字段。
            pd_action['page_extra'] = pd_action['page_extra'].map(lambda x: [('no_page', -1)] if type(x) != list else x)
            pd_action['module_extra'] = pd_action['module_extra'].map(lambda x: [('no_page', -1)] if type(x) != list else x)
            pd_action['activity_index'] = pd_action[['platform', 'page_extra', 'module_extra', 'module_list_index', 'module_list_len']]\
                .apply(lambda x: get_activity_index(x), axis=1)
            # 点击数据写为True
            pd_action.loc[:, 'label'] = 1
            pd_action = pd_action.rename(columns={'module_object_id': 'activity_id'})
            pd_action = pd_action[['label', 'activity_index']+output_features].drop_duplicates()

    # 按page_open_id获取正负样本及去重
    iht_data = pd.merge(pd_exposure, pd_action, on=['activity_index'] + output_features, how='left')
    iht_data['label'].fillna(0, inplace=True)

    # page_open_id 去重及过滤<page_open_id,activity,activity_index>不unique的活动。
    # 三个过滤条件能过滤10%左右的数据<注：以下比例以2021.03.24当天为例>
    # unique_page_open_id_1:删除存在多种'device_id', 'uid', 'platform', 'language', 'ip_country_code', 'ip_city_name' 的page_open_id，占总数据0.28%。
    unique_page_open_id_1 = iht_data[['page_open_id', 'device_id', 'uid', 'platform', 'language', 'ip_country_code','ip_city_name']].drop_duplicates() \
        .groupby(['page_open_id']).size().reset_index(name='nums').query('nums==1')[['page_open_id']]
    iht_data = pd.merge(iht_data, unique_page_open_id_1, on=['page_open_id'])

    # unique_page_open_id_2:删除同个page_open_id，同个module_list_index下有多个activity_id的活动，占总数据8.3%。《如果直接删除整个page_open_id数据丢失比例太高，20%左右》
    unique_page_open_id_2 = iht_data[['page_open_id', 'activity_index', 'activity_id']].drop_duplicates().groupby(['page_open_id', 'activity_index']) \
        .size().reset_index(name='nums').query('nums==1')[['page_open_id', 'activity_index']]
    iht_data = pd.merge(iht_data, unique_page_open_id_2, on=['page_open_id', 'activity_index'])

    # unique_page_open_id_3:删除同个page_open_id，同个activity_id下有多个module_list_index的活动，占总数据4.8%。《如果直接删除整个page_open_id数据丢失比例太高，20%左右》
    unique_page_open_id_3 = iht_data[['page_open_id', 'activity_index', 'activity_id']].drop_duplicates().groupby(['page_open_id', 'activity_id']) \
        .size().reset_index(name='nums').query('nums==1')[['page_open_id', 'activity_id']]
    iht_data = pd.merge(iht_data, unique_page_open_id_3, on=['page_open_id', 'activity_id'])

    # 删除每个page_oepn_id最后一个点击后的曝光数据及没有点击的page_open_id
    group_id_df = iht_data.query('label==1 and activity_index>0')[['page_open_id', 'activity_index']].rename(columns={'activity_index': 'click_activity_index'})
    group_id_df['click_rank'] = group_id_df.groupby(by=['page_open_id'])['click_activity_index'].rank(ascending=True, method='dense')
    group_id_df = group_id_df.query('click_rank==1')[['page_open_id', 'click_activity_index']]
    iht_data = pd.merge(iht_data, group_id_df, on=['page_open_id']).query('activity_index<=click_activity_index')

    # activity_id必须为数值型
    iht_data = iht_data[pd.to_numeric(iht_data['activity_id'], errors='coerce').notnull()]
    iht_data['activity_id'] = iht_data['activity_id'].astype(str)
    logging.info('%s sample data loading finished ' % dateStr)
    return iht_data[output_features+['label']]


def load_booking_data(dateStr, windows=30):
    """加载booking date_str前30天数据

    :param cur_date_str: 加载日期
    """
    logging.info('loading %s booking data ...' % dateStr)
    input_columns = ['booking_id', 'global_id', 'activity_id', 'order_create_time_utc']
    cur_date = datetime.datetime.strptime(dateStr, "%Y-%m-%d")
    tor_date = cur_date + datetime.timedelta(days=+3)  # 避免utc与utc+8时间误差
    tor_date_str = tor_date.strftime("%Y-%m-%d")
    last_month_date = cur_date + datetime.timedelta(days=-(windows+3))  # 避免utc与utc+8时间误差
    last_month_date_str = last_month_date.strftime("%Y-%m-%d")
    # 加booing数据
    sh_booking = "aws s3 cp s3://klook-big-data-prod/data/processed/rds/data_integration/booking \
                       ./dataset/booking/ --recursive  > cp.log "
    sh_delete_null_file = "find ./dataset/* -name \"*\" -type f -size 0c  | xargs -n 1 rm -f"
    booking_source = pd.DataFrame()
    if os.system(sh_booking) == 0:
        if os.system(sh_delete_null_file) == 0:
            booking_source = pd.read_parquet('./dataset/booking/', columns=input_columns)
            # 删除本地文件，释放磁盘空间
            sh_delete_file = 'rm -rf ./dataset/booking/'
            os.system(sh_delete_file)
            booking_source['order_create_time_utc'] = pd.to_datetime(booking_source['order_create_time_utc'])
            booking_source['date'] = booking_source['order_create_time_utc'].dt.date.astype(str)
            booking_source['date'] = booking_source['date'].astype('str')
            booking_source = booking_source.query('date>=@last_month_date_str').query('date<=@tor_date_str')
            booking_source = booking_source[['date', 'global_id', 'activity_id']].drop_duplicates()
            booking_source['activity_id'] = booking_source['activity_id'].astype('str')
            booking_source.rename(columns={'global_id': 'uid'}, inplace=True)
            booking_source['booking_label'] = 2
    logging.info('%s booking data loading finished ' % dateStr)
    return booking_source