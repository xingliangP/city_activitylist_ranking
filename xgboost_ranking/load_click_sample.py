import os
import pandas as pd
from __init__ import get_activity_index
import logging
pd.options.mode.chained_assignment = None

dst_prod = "s3://klook-big-data-prod/data/processed"


def load_click_data(dateStr):
    """加载城市活动页面dateStr点击样本,主要用于预测

    :param dateStr: 样本日期
    """
    logging.info('loading %s sample data ...' % dateStr)
    year, month, day = dateStr.split("-")
    dict_lang = {'de': 'de_DE', 'es': 'es_ES', 'fr': 'fr_FR', 'id': 'id_ID', 'it': 'it_IT', 'ja': 'ja_JP',
                 'ru': 'ru_RU', 'vi': 'vi_VN', 'th': 'th_TH', 'ko': 'ko_KR', 'en': 'en_BS'}
    # 数据读取过滤--数据加载时过滤，减小数据量，提升整体处理速度
    data_source_1 = [('spm_page', 'in', ("City", "CityActivityListing")), ('spm_module', '=', 'Activity_LIST'),
                     ('module_object_id_type', '=', 'activity'), ('module_object_id', '!=', ''), ('site_name', 'in', ("klook", "www.klook.com"))]
    # 数据加载后过滤 --这部分数据量比较少，加载后过滤
    data_filter_1 = 'device_id.notnull() and device_id !=""  and page_open_id.notnull() and page_open_id !=""'
    # 正负样本公共特征--输入特征名称
    input_columns = ["device_id", "uid", "platform", "language", "ip_country_code", 'ip_city_name', 'module_object_id', 'page_open_id',
                     'page_extra', 'module_extra', 'module_list_index', 'module_list_len']
    # 正负样本公共特征 --输出特征名称 + 'label'
    output_features = ['page_open_id', 'activity_id', 'device_id', 'uid', 'platform', 'language', 'ip_country_code', 'ip_city_name']

    # sh--删除空文件夹
    sh_delete_null_file = "find ./dataset/* -name \"*\" -type f -size 0c  | xargs -n 1 rm -f"
    # sh--下载点击数据
    sh_click_download = "aws s3 cp {dst}/in-house-tracking/event_click/year={year}/month={month}/day={day}/ \
                       ./dataset/event_click/year={year}/month={month}/day={day} --recursive > cp.log".format(dst=dst_prod, year=year, month=month, day=day)
    pd_action = pd.DataFrame()
    if os.system(sh_click_download) == 0:
        if os.system(sh_delete_null_file) == 0:
            cur_path = './dataset/event_click/year={year}/month={month}/day={day}'.format(year=year, month=month, day=day)
            # 取城市数据
            pd_action = pd.read_parquet(cur_path, columns=input_columns, filters=data_source_1)
            # 删除本地文件，释放磁盘空间
            sh_delete_file = 'rm -rf ./dataset/event_click/'
            os.system(sh_delete_file)
            pd_action = pd_action.query(data_filter_1, engine='python')
            # 语言多端统一
            pd_action.loc[:, 'language'] = pd_action['language'].map(lambda x: x.replace("-", "_")).map(lambda x: dict_lang[x] if x in dict_lang else x)
            # 点击数据写为True
            pd_action.loc[:, 'label'] = 1
            pd_action = pd_action.rename(columns={'module_object_id': 'activity_id'})
            pd_action = pd_action[output_features].drop_duplicates()
    return pd_action[output_features]
