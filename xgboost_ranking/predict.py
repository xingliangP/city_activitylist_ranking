from __init__ import get_activity_index, load_model,  get_dcg_res
import load_click_sample, load_city_list_sample
import pandas as pd
from iht_features import feature_warehouse
import datetime
import numpy as np
from iht_features.labelEncoder import load_dictionary, encoder
from xgboost import DMatrix



date_str_in = '2021-03-25'
# 读取当天点击数据
iht_data = load_click_sample.load_click_data(date_str_in)[['device_id', 'uid', 'platform', 'language', 'activity_id']].drop_duplicates()
dateRaw = datetime.datetime.strptime(date_str_in, "%Y-%m-%d")

# 当天有uid 的device_id，确保其所有点击数据都有uid，便于加上booking_label
device_uid = iht_data.query('uid.notnull() and uid !=""', engine='python')[['device_id', 'uid']].drop_duplicates()
iht_data = pd.merge(iht_data.drop(columns=['uid']), device_uid, how='left')

# 加上booking_label
booking_base = load_city_list_sample.load_booking_data(date_str_in).rename(columns={'booking_label': 'label'})
iht_data = pd.merge(iht_data, booking_base.query('date==@date_str_in')[['uid', 'activity_id', 'label']], on=['uid', 'activity_id'], how='left')
iht_data['label'].fillna(1, inplace=True)

# 获取商品静态特征；输入的时间需要做T-1
act_base_columns = ['activity_id', 'activity_country_id', 'activity_city_id', 'has_video', 'language',
                    'taxon_category_id', 'taxon_sub_category_id', 'taxon_leaf_category_id']
act_base = feature_warehouse.activity_base_features(date_str_in, columns=act_base_columns)
act_base['activity_id'] = act_base['activity_id'].astype(str)

# 获取活动发布状态；现有的s3数据没有活动名和活动状态，需要手动上传bigquery表数据到s3对应路径
act_base_bq = pd.read_csv('s3://sagemaker-studio-227729285155-ml/hubery-peng/bigquery_data/actvity_base/act_base_bq_2021_03_25_2021_03_26.csv').query('date==@date_str_in')
act_base_bq['activity_id'] = act_base_bq['activity_id'].astype(str)
act_base = pd.merge(act_base, act_base_bq, on=['activity_id'])
iht_data = pd.merge(iht_data, act_base, on=['activity_id', 'language'], how='left')

# 只对队头部营收城市进行离线实验
# city_language = {2: ['en_SG', 'en_BS', 'en_GB', 'en_US'], 6: ['zh_HK', 'en_BS', 'en_HK', 'zh_TW'],
#                  43: ['zh_TW'], 78: ['en_BS', 'en_GB', 'ru_RU'], 19: ['zh_TW', 'en_SG', 'en_BS', 'zh_HK'],
#                  25: ['zh_TW', 'en_SG', 'en_BS']}

iht_data['activity_city_id'] = iht_data['activity_city_id'].astype('str')
city_2 = iht_data.query('activity_city_id=="2"')
city_6 = iht_data.query('activity_city_id=="6"')
city_19 = iht_data.query('activity_city_id=="19"')
check_sample = pd.concat([city_2, city_6, city_19], ignore_index=True)
check_sample = check_sample[['device_id', 'platform', 'language', 'label', 'activity_city_id']].drop_duplicates()
act_base['activity_city_id'] = act_base['activity_city_id'].astype('str')
check_sample = pd.merge(check_sample, act_base.query('activity_city_id in ["2","6","19"]'), on=['activity_city_id', 'language'])
# 只对每个城市的top200 进行排序。
# 线上排序分
booking_sorce = pd.read_csv('dataset/city_online_raning/2021_03_25_30d_bookings.csv')
booking_sorce['popular_rank'] = booking_sorce.groupby('city_id')['booking_num'].rank(ascending=False, method='first')
booking_sorce['city_id'] = booking_sorce['city_id'].astype(str)
booking_sorce['activity_id'] = booking_sorce['activity_id'].astype(str)
check_sample = pd.merge(check_sample, booking_sorce.query('popular_rank<=200')[['activity_id']])

# 获取商品动态特征；活动的action需要做滑窗，输入的时间需要做t-1
date_action_in = dateRaw + datetime.timedelta(days=-1)
date_action_str = date_action_in.strftime("%Y-%m-%d")
data_query = 'spm_page in ("City", "CityActivityListing") and spm_module == "Activity_LIST"'
act_action_weekly = feature_warehouse.activity_action_features(date_action_str, 7, feature_name_suffix='weekly_recomCity', query=data_query)
act_action_monthly = feature_warehouse.activity_action_features(date_action_str, 30, feature_name_suffix='monthly_recomCity', query=data_query)
check_sample = pd.merge(check_sample, act_action_weekly, on=['activity_id'], how='left')
check_sample = pd.merge(check_sample, act_action_monthly, on=['activity_id'], how='left')

# 获取用户画像数据； 输入的时间无需要做T-1，该表数据在生成时已为T-1
profile_columns = ['device_id', 'last_day_favorite_leaf_category', 'last_month_favorite_leaf_category']
user_profile = feature_warehouse.device_features(date_str_in, columns=profile_columns)
check_sample = pd.merge(check_sample, user_profile, on=['device_id'], how='left')
check_sample['last_day_favorite_leaf_category_top_1'] = check_sample['last_day_favorite_leaf_category'].replace(np.nan, '0').replace('', '0') \
    .map(lambda x: str(x).split(',')[0]).astype(int)
check_sample['last_month_favorite_leaf_category_top_1'] = check_sample['last_month_favorite_leaf_category'].replace(np.nan, '0').replace('', '0') \
    .map(lambda x: str(x).split(',')[0]).astype(int)

# 缺失值补全
default_values = {"platform": '',
                  "language": '',
                  'activity_country_id': 0,
                  'activity_city_id': 0,
                  'taxon_category_id': 0,
                  'taxon_sub_category_id': 0,
                  'taxon_leaf_category_id': 0}
check_sample.fillna(value=default_values, inplace=True)

# 以下字段fillna不能替换缺失值，需要用replace，后期定位原因
fillna_col = ['exposure_num_weekly_recomCity', 'click_num_weekly_recomCity', 'shopping_num_weekly_recomCity',
              'favourite_num_weekly_recomCity', 'booking_num_weekly_recomCity', 'average_review_score_weekly_recomCity',
              'exposure_num_monthly_recomCity', 'click_num_monthly_recomCity', 'shopping_num_monthly_recomCity',
              'favourite_num_monthly_recomCity', 'booking_num_monthly_recomCity',
              'average_review_score_monthly_recomCity']
for col in fillna_col:
    check_sample[col].replace(np.nan, 0.0, inplace=True)
check_sample['has_video'].replace(np.nan, False, inplace=True)

# 类别特征编码
labelEncoder_dict = load_dictionary()
check_sample = encoder(check_sample, labelEncoder_dict)

model_path = 'dataset/models/local_train/2021-02-22_2021-03-24/'
model, feature_names = load_model(model_path)
# xgboost打分
# 字段类型转换
columns = [column for column in feature_names if
           column not in ["average_review_score_weekly_recomCity", "average_review_score_monthly_recomCity"]]
for col in columns:
    check_sample[col] = check_sample[col].astype(int)
check_sample['average_review_score_weekly_recomCity'] = check_sample['average_review_score_weekly_recomCity'].astype(float)
check_sample['average_review_score_monthly_recomCity'] = check_sample['average_review_score_monthly_recomCity'].astype(float)
check_sample.to_parquet('dataset/offline_check_data/2021_03_25_check_sample.parquet')

check_data = DMatrix(check_sample[feature_names], missing=0)
test_rank_scores = model.predict(check_data)

# 预测模型
out_put_data = check_sample.copy(deep=True)
out_put_data['ranking_score'] = pd.Series(test_rank_scores)
out_put_data['score_rank'] = out_put_data.groupby(['device_id', 'platform', 'language', 'activity_city_id'])['ranking_score'].rank(ascending=False,method='first')

raw_res = iht_data[['device_id', 'activity_id', 'platform',  'language', 'label']].drop_duplicates()
raw_res = iht_data[['device_id', 'activity_id', 'platform', 'language', 'label']].drop_duplicates()
# 类别特征编码
labelEncoder_dict = load_dictionary()
raw_res = encoder(raw_res, labelEncoder_dict)
# 生成预测结果
# xgboost组
exp_res = pd.merge(out_put_data[['device_id', 'activity_id', 'platform', 'language', 'exposure_num_weekly_recomCity', 'click_num_weekly_recomCity',
                                 'exposure_num_monthly_recomCity',  'click_num_monthly_recomCity', 'booking_num_weekly_recomCity',
                                 'booking_num_monthly_recomCity', 'ranking_score', 'score_rank']].drop_duplicates(),\
             raw_res[['device_id', 'platform', 'language', 'activity_id', 'label']].drop_duplicates(),\
             on = ['device_id', 'platform', 'language', 'activity_id'], how='left')\
            .sort_values(by=["device_id", "ranking_score"], ascending=[True,False])\
            [['label', 'device_id', 'platform', 'language', 'activity_id',\
              'exposure_num_weekly_recomCity', 'click_num_weekly_recomCity',\
              'exposure_num_monthly_recomCity', 'click_num_monthly_recomCity','booking_num_weekly_recomCity',\
              'booking_num_monthly_recomCity', 'ranking_score', 'score_rank']]
exp_res['label'].fillna(0, inplace=True)
exp_res['ctr_weekly'] = exp_res['click_num_weekly_recomCity']/exp_res['exposure_num_weekly_recomCity']
exp_res['ctr_monthly'] = exp_res['click_num_monthly_recomCity']/exp_res['exposure_num_monthly_recomCity']
exp_res['cvr_weekly'] = exp_res['booking_num_weekly_recomCity']/exp_res['click_num_weekly_recomCity']
exp_res['cvr_monthly'] = exp_res['booking_num_monthly_recomCity']/exp_res['click_num_monthly_recomCity']
exp_res.fillna(0, inplace=True)

# default组
control_online = booking_sorce.query('city_id in ["2","6","19"] and popular_rank<=200')
control_online.rename(columns={'popular_rank': 'score_rank'}, inplace=True)
control_data = check_sample.copy(deep=True)
control_data = pd.merge(control_data, control_online)
def_res = pd.merge(control_data[['device_id', 'activity_id', 'platform', 'language', 'exposure_num_weekly_recomCity','click_num_weekly_recomCity',
                                 'exposure_num_monthly_recomCity', 'click_num_monthly_recomCity','booking_num_weekly_recomCity',
                                 'booking_num_monthly_recomCity', 'score_rank']].drop_duplicates(),\
             raw_res[['device_id', 'platform', 'language', 'activity_id', 'label']].drop_duplicates(),\
             on = ['device_id', 'platform', 'language', 'activity_id'], how='left')\
            [['label', 'device_id', 'platform', 'language', 'activity_id',\
              'exposure_num_weekly_recomCity', 'click_num_weekly_recomCity',\
              'exposure_num_monthly_recomCity', 'click_num_monthly_recomCity', 'booking_num_weekly_recomCity',\
              'booking_num_monthly_recomCity', 'score_rank']]
def_res['label'].fillna(0, inplace=True)
def_res['ctr_weekly'] = def_res['click_num_weekly_recomCity']/def_res['exposure_num_weekly_recomCity']
def_res['ctr_monthly'] = def_res['click_num_monthly_recomCity']/def_res['exposure_num_monthly_recomCity']
def_res['cvr_weekly'] = def_res['booking_num_weekly_recomCity']/def_res['click_num_weekly_recomCity']
def_res['cvr_monthly'] = def_res['booking_num_monthly_recomCity']/def_res['click_num_monthly_recomCity']
def_res.fillna(0, inplace=True)

# 返回dcg结果
def_res['label_click'] = def_res['label'].replace(2, 1)
exp_res['label_click'] = exp_res['label'].replace(2, 1)
def_res['label_booking'] = def_res['label'].replace(1, 0)
exp_res['label_booking'] = exp_res['label'].replace(1, 0)
new_res = pd.DataFrame(get_dcg_res(exp_res, def_res, ['label', 'label_click', 'label_booking', 'ctr_weekly', 'cvr_weekly', 'ctr_monthly', 'cvr_monthly'], 'xgb'))
