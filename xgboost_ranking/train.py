# coding: utf-8

import os
import logging
import pickle
import pandas as pd
import datetime
from matplotlib import pyplot as plt
from xgboost import DMatrix, train
from xgboost import plot_importance
from data import load_train_data
import time
import argparse
from __init__ import train_model, generate_dataset , print_result


def main(**params):
    # 生成训练集
    logging.info("generate train set...")
    cur_date = params['cur_date']
    days_num = int(params['days_num'])
    # 特征顺序
    label_features = ["label", "page_open_id"]
    online_features = ['platform', 'language']
    device_features = ['last_day_favorite_leaf_category_top_1', 'last_month_favorite_leaf_category_top_1']
    user_features = ['']
    act_features = ['activity_country_id', 'activity_city_id', 'has_video', 'taxon_category_id',
                    'taxon_sub_category_id', 'taxon_leaf_category_id',
                    'exposure_num_weekly_recomCity', 'click_num_weekly_recomCity', 'shopping_num_weekly_recomCity',
                    'favourite_num_weekly_recomCity', 'booking_num_weekly_recomCity', 'average_review_score_weekly_recomCity',
                    'exposure_num_monthly_recomCity', 'click_num_monthly_recomCity', 'shopping_num_monthly_recomCity',
                    'favourite_num_monthly_recomCity', 'booking_num_monthly_recomCity','average_review_score_monthly_recomCity']

    # 加载训练数据
    (train_data, test_data) = load_train_data(cur_date, days_num)

    # 将数据处理成模型能加载的格式
    dtrain = generate_dataset(train_data[label_features+online_features+device_features+act_features])
    dtest = generate_dataset(test_data[label_features+online_features+device_features+act_features])
    evals = [(dtest, "prod")]

    # 设置模型保存路径---保持模型路径为训练集合的起始时间
    dateRaw = datetime.datetime.strptime(cur_date, "%Y-%m-%d")
    start_data = dateRaw + datetime.timedelta(days=-days_num)
    start_data = start_data.strftime("%Y-%m-%d")
    model_prefile = start_data + '_' + cur_date
    model_path = "./results/models/"+model_prefile+"_baseline_v2"
    if not os.path.exists(model_path):
        os.makedirs(model_path)
    start_time = time.time()
    save_model_file = os.path.join(model_path, "xgboost.model")

    # 开始模型训练
    results, model, model_params = train_model(dtrain, evals, save_model_file, True)
    # 打印训练指标
    eval_result = print_result(results)
    end_time = time.time()
    logging.info('time is %s ' % round((end_time - start_time) / 60, 3))

    # 保存训练结果
    df_metrics = pd.DataFrame(eval_result[0]['metrics_items'])
    df_metrics['训练集合时间区间'] = model_prefile
    mer_columns = df_metrics.columns.tolist()

    mer_columns = [column for column in mer_columns if column not in ["训练集合时间区间"]]
    mer_columns = ["训练集合时间区间"] + mer_columns
    df_metrics[mer_columns].to_csv(model_path + '/metrics.csv', index=False)

    # 保存模型特征权重
    fig, ax = plt.subplots(figsize=(10, 8))
    plot_importance(model,
                    height=0.5,
                    ax=ax,
                    max_num_features=64)
    features_importance_path = os.path.join(model_path, "features_importance.png")
    plt.savefig(features_importance_path, dpi=200, bbox_inches='tight')
    plt.show()

    # 保存将特征名及特征顺序
    feature_names_file = os.path.join(model_path, "feature_names.pkl")
    with open(feature_names_file, "wb") as f:
        feature_names = train_data[online_features+device_features+act_features].columns.values.tolist()
        pickle.dump(feature_names, f)
    logging.info("save feature names to %s." % feature_names_file)

    # 保存一份config格式字段名 用于在线工程调用
    feaure_order = {'feature_type': ['online', 'device', 'user', 'activity'],
                    'feature_name': [','.join(online_features), ','.join(device_features),','.join(user_features), ','.join(act_features)]}
    pd.DataFrame(feaure_order).to_csv(model_path+'/config.csv')

    logging.info("finished.")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    yesterday = datetime.datetime.now() + datetime.timedelta(days=-1)
    yesterday_str = yesterday.strftime("%Y-%m-%d")
    parser.add_argument("--cur_date", default=yesterday_str, help="train last date", type=str)
    parser.add_argument("--days_num", default="30", help="train duration", type=str)
    args = parser.parse_args()
    main(**{"cur_date": args.cur_date, "days_num": args.days_num})